package com.xlrs.schedular.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xlrs.schedular.exception.ApplicationException;
import com.xlrs.schedular.view.CustomerAccountView;
import com.xlrs.schedular.view.IllionUserLoginView;
import com.xlrs.schedular.view.ResponseView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IllionCustomerBankDataRestTemplate {
	
	
	private static final RestTemplate restTemplate = new RestTemplate();
	
	public static final String API_KEY = "X-API-KEY";
	
	String serverport = "13.239.118.86";
	
	private String insertCustomerBankDataURL= "http://"+serverport+":4008/xlrs/customerdata/insert";
	private String getAlLoginUsersURL = "http://"+serverport+":4008/xlrs/illionUsers/all";
	
	public  List<IllionUserLoginView> getAllLoginUsers() throws NoSuchMessageException, ApplicationException {
		 List<IllionUserLoginView> illionUserLoginViews =null;
		try {
			ResponseView responseView = get(getAlLoginUsersURL, ResponseView.class);
			if(responseView.getErrorMessages()==null) {
				illionUserLoginViews =  new ObjectMapper().convertValue(responseView.getData(), new TypeReference<List<IllionUserLoginView>>(){});
			}
			log.debug("Successfully fetched Illion System User Data : size is " + illionUserLoginViews.size());
		} catch (Exception e) {
			throw new ApplicationException(e.getMessage(),e);
		}
		return illionUserLoginViews;
	}
	
	
	public JSONObject insertCustomerBankData(CustomerAccountView customerAccountView) throws NoSuchMessageException, ApplicationException {
		JSONObject customerDataResponse =null;
		
		log.debug("Fetching records for customer : : "+ customerAccountView.toString());
		
		try {
			ResponseView responseView = postWithHeaders(insertCustomerBankDataURL, customerAccountView, getHeaders(), ResponseView.class);
			if(responseView.getErrorMessages()==null) {
				customerDataResponse =  new ObjectMapper().convertValue(responseView.getData(), JSONObject.class);
			}
			log.info("Successfully fetched Illion Customer Bank Data");
		} catch (Exception e) {
			throw new ApplicationException(e.getMessage(),e);
		}
		return customerDataResponse;
	}
	
	private ResponseView postWithHeaders(String url, Object requestObject, Map<String, String> headers, Class<ResponseView> responseType) {
		HttpHeaders tempHeader = new HttpHeaders();
		headers.forEach((key, value) -> {
			tempHeader.set(key, value);
		});
		HttpEntity<Object> entity = new HttpEntity<>(requestObject, tempHeader);
		ResponseView response = restTemplate.postForObject(url, entity, responseType);
		return response;
	}

	
	private Map<String, String> getHeaders() {
		Map<String,String> headers = new HashMap<String, String>();
		headers.put(API_KEY, "f66778e6-e866-4e3c-9c1d-3687223640bc");
		return headers;
	}
	
	private ResponseView get(String url, Class<ResponseView> t, Object... params) {
		return (ResponseView) restTemplate.getForObject(url, t, params);
	}

}
